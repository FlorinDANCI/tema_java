/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 *
 * @author Florin
 */

public class ViewClass extends JFrame{
    private final JButton btnResult;
    private final JTextField txtNumber;
    private final JLabel lblNumber;
    private final JLabel lblResult;
    
    public ViewClass(int width, int heigth){
        //Frame
        this.setTitle("TemaCV_Pentastagiu");
        this.setPreferredSize(new Dimension(width, heigth));
        this.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        JPanel mainPan = new JPanel();
        mainPan.setLayout(new GridLayout(4, 1, 2, 2));
        
        //Label and TextField
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        lblNumber = new JLabel("Enter a number:");
        txtNumber = new JTextField(10);
        panel.add(lblNumber);
        panel.add(txtNumber);
        mainPan.add(panel);
        
        //Buttons
        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        btnResult = new JButton("Result");
        panel2.add(btnResult);
        mainPan.add(panel2);
        
        //Result
        JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        lblResult = new JLabel("Result will be here");
        panel3.add(lblResult);
        mainPan.add(panel3);
        
        //Result
        JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.CENTER)); 
        JLabel lblDescription = new JLabel();
        lblDescription.setText("Press \"Result\" to find out the biggest digit!");
        lblDescription.setForeground(Color.black);
        lblDescription.setFont(new Font("Arial", Font.BOLD, 13));
        panel4.add(lblDescription);
        mainPan.add(panel4);
        
        this.add(mainPan);
        this.pack();
    }
    
    public JButton GetButton(){
        return btnResult;
    }
    
    public void setResultColor(int color){
        switch(color){
            case 1:
                lblResult.setForeground(Color.RED);
                break;
            case 2:
                lblResult.setForeground(Color.GREEN);
                break;
            default:
                //Do something..
                break;
        }
    }
    
    public void SetResult(String result){
        lblResult.setText(result);
    }
    
    public int GetInput(){
        String sInput = txtNumber.getText();
        return Integer.valueOf(sInput);
    }
    
    public void SetErrorMsg(String msg){
        lblResult.setText(msg);
    }
}
