/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author Florin
 */
public class ModelClass {

    private final int n;

    public ModelClass(int n) {
        this.n = n;
    }

    public int getNumber(){
        return this.n;
    }
    
    public int MaxDigit(int n) {
        n = Math.abs(n);
        if (n > 0) {
            int digit = n % 10;
            int max = MaxDigit(n / 10);
            return Math.max(digit, max);
        } else {
            return 0;
        }
    }
}
