/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Florin
 */
public class ControllerClass implements ActionListener {

    ViewClass v;
    ModelClass m;

    public ControllerClass() {
        v = new ViewClass(300, 200);
        v.setVisible(true);

        v.GetButton().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (v.GetButton() == ae.getSource()) {
            try {
                m = new ModelClass(v.GetInput());
                int number = m.getNumber();
                
                String res = String.valueOf(m.MaxDigit(number));
                v.setResultColor(2); //TODO change to enum
                v.SetResult(res);
            } catch (Exception ex) {
                v.setResultColor(1); //TODO change to enum
                v.SetErrorMsg("ERROR");
            }
        }
    }
}
